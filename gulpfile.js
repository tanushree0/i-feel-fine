
let gulp = require('gulp'),
  sass = require('gulp-sass'),
  autoprefixer = require('gulp-autoprefixer');

/* Compile main style.scss */
gulp.task('styles-maincss', function() {
  return gulp.src('src/index.scss')
    .pipe(sass({
      /* outputStyle: 'compressed',*/
      includePaths: ['node_modules/susy/sass']
    }))
    .pipe(gulp.dest('src'));
});

gulp.task('autoprefixer', function() {
  return gulp.src('src/style.css')
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(gulp.dest('src'));
});


/*
 * Default task that runs when you type 'gulp' in the terminal.
 * */
gulp.task('default',function() {
  gulp.watch(['src/index.scss', 'src/styles/**/*'],['styles-maincss']);
  gulp.watch(['src/index.css'], ['autoprefixer']);
});