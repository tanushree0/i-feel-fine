import firebase from 'firebase';

const config = {
    apiKey: "AIzaSyBiAlr5Z3S1TDJr9MFF5Khp6-EDoieKjy4",
    authDomain: "i-feel-fine.firebaseapp.com",
    databaseURL: "https://i-feel-fine.firebaseio.com",
    projectId: "i-feel-fine",
    storageBucket: "",
    messagingSenderId: "365368498098"
};

firebase.initializeApp(config);

export const provider = new firebase.auth.GoogleAuthProvider();
export const auth = firebase.auth();
export default firebase;