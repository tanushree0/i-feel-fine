import React, { Component } from 'react';
import './index.css';

import firebase, { auth, provider } from './firebase.js';

class App extends Component {

  constructor() {
    super();
    this.state = {
        user: null
    };

    this.login = this.login.bind(this);
    this.logout = this.logout.bind(this);
  }


  render() {

      // const itemsRef = firebase.database().ref('items');
      // const item = {
      //     feelings: ['happy', 'sad', 'disgust', 'angry']
      // };
      // itemsRef.push(item);

    return (
      <div className="App">
        <header className="App-header">
          <span className="logo">i feel fine</span>

          <nav>
            <ul>
              <li>{this.state.user ? <a onClick={this.logout}>Logout</a> : <a onClick={this.login}>Login</a>}</li>
            </ul>
          </nav>

          {this.state.user &&
           <div>
             <img className='user-profile-pic' src={this.state.user.photoURL} />
           </div>
          }

        </header>

        <main>
          <Home/>
        </main>
        <footer>

        </footer>
      </div>
    );
  }

  componentDidMount() {
    auth.onAuthStateChanged((user) => {
      if (user) {
        this.setState({user});
      }
    });
  }

  login() {
    auth.signInWithPopup(provider)
      .then((result) => {
        const user = result.user;
        this.setState({
          user
        });
      });
  }

  logout() {
    auth.signOut()
      .then(() => {
        this.setState({
          user: null
        });
      });
  }
}

export default App;

class Home extends Component {

  render() {
    return(
      <div>
        <p>How do you feel today?</p>
        <button>Google Login</button>
      </div>
    );
  }
}
