The CSS code is organised in the following folders:

1. Vendor
Contains third-party CSS files.

2. Base
Contains base styles used throughout the project

3. Components
CSS for components. Will often directly map to React components.

4. Pages
